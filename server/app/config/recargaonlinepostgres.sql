BEGIN;
CREATE TABLE profile (
	id SERIAL
	,name CHARACTER VARYING(255)
        ,createddate TIMESTAMP WITH TIME ZONE DEFAULT NOW()
	,PRIMARY KEY (id)
	);

CREATE TABLE module (
	id SERIAL
	,name CHARACTER VARYING(255)
        ,createddate TIMESTAMP WITH TIME ZONE DEFAULT NOW()
	,PRIMARY KEY (id)
	);

CREATE TABLE operator (
	id SERIAL
	,name CHARACTER VARYING(255)
        ,createddate TIMESTAMP WITH TIME ZONE DEFAULT NOW()
	,PRIMARY KEY (id)
	);

CREATE TABLE plan (
	id SERIAL
	,NAME CHARACTER VARYING(255)
	,operator_id INTEGER
        ,createddate TIMESTAMP WITH TIME ZONE DEFAULT NOW()
	,PRIMARY KEY (id)
	,FOREIGN KEY (operator_id) REFERENCES operator(id) ON DELETE CASCADE
        --,FOREIGN KEY (destination_operator_id) REFERENCES operator(id) ON DELETE CASCADE
	);

CREATE TABLE "user" (
	id SERIAL
	,profile_id INTEGER
	,email CHARACTER VARYING(255) UNIQUE
	,password CHARACTER VARYING(255)
        ,api_key CHARACTER VARYING(255)
	,active boolean DEFAULT true
	,createddate TIMESTAMP WITH TIME ZONE DEFAULT NOW()
	,PRIMARY KEY (id)
	,FOREIGN KEY (profile_id) REFERENCES PROFILE (id) ON DELETE SET NULL
	);

CREATE TABLE api_key (
    id SERIAL
    ,user_id INTEGER
    ,apikey CHARACTER VARYING(255)
    ,expireddate TIMESTAMP WITH TIME ZONE DEFAULT NOW()
    ,PRIMARY KEY (id)
    ,FOREIGN KEY (user_id) REFERENCES "user"(id) ON DELETE CASCADE
);

CREATE TABLE phonenumber (
	id SERIAL
	,number CHARACTER VARYING(255) UNIQUE
	,operator_id INTEGER
	,plan_id INTEGER
	,user_id INTEGER
	,balance NUMERIC(15, 2) DEFAULT 0
        ,createddate TIMESTAMP WITH TIME ZONE DEFAULT NOW()
	,PRIMARY KEY (id)
	,FOREIGN KEY (operator_id) REFERENCES operator(id) ON DELETE SET NULL
	,FOREIGN KEY (plan_id) REFERENCES PLAN (id) ON DELETE SET NULL
	,FOREIGN KEY (user_id) REFERENCES "user"(id) ON DELETE CASCADE
	);
	
CREATE TABLE plandetail (
	id SERIAL
	,plan_id INTEGER
        ,operator_id INTEGER
	,destination_operator_id INTEGER
	,cost NUMERIC(15,2) DEFAULT 0
        ,createddate TIMESTAMP WITH TIME ZONE DEFAULT NOW()
	,PRIMARY KEY(id)
	,FOREIGN KEY (plan_id) REFERENCES plan(id) ON DELETE CASCADE
	,FOREIGN KEY (destination_operator_id) REFERENCES operator(id) ON DELETE SET NULL
	);

CREATE TABLE recharge (
		id SERIAL
		,phonenumber_id INTEGER
		--,plan_id INTEGER
		,balance NUMERIC(15,2)
                ,createddate TIMESTAMP WITH TIME ZONE DEFAULT NOW()
		,PRIMARY KEY (id)
		,FOREIGN KEY (phonenumber_id) REFERENCES phonenumber(id) ON DELETE CASCADE
		--,FOREIGN KEY (plan_id) REFERENCES plan(id)
		);
		
CREATE TABLE consumption (
		id SERIAL
		,origin_phonenumber_id INTEGER 
		,destiny_phonenumber_id INTEGER	
                ,operator_id INTEGER	
		,call_duration TIME
		,balance_spent NUMERIC(15,2)
                ,createddate TIMESTAMP WITH TIME ZONE DEFAULT NOW()
		,PRIMARY KEY (id)
		,FOREIGN KEY (origin_phonenumber_id) REFERENCES phonenumber(id) ON DELETE CASCADE
		,FOREIGN KEY (destiny_phonenumber_id) REFERENCES phonenumber(id) ON DELETE SET NULL
                ,FOREIGN KEY (operator_id) REFERENCES operator(id) ON DELETE SET NULL
		--,FOREIGN KEY (plan_id) REFERENCES plan(id)
		);
CREATE TABLE permissionmodule (
	id SERIAL
	,module_id INTEGER
	,profile_id INTEGER
	,create_p BOOLEAN DEFAULT false
	,read_p BOOLEAN DEFAULT false
	,update_p BOOLEAN DEFAULT false
	,delete_p BOOLEAN DEFAULT false
	,PRIMARY KEY (id)
	,FOREIGN KEY (module_id) REFERENCES module(id) ON DELETE CASCADE
	,FOREIGN KEY (profile_id) REFERENCES profile(id) ON DELETE CASCADE
	);		
	
COMMIT;