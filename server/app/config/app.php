<?php
namespace recargaonline;
date_default_timezone_set('America/Los_Angeles');
define('ROOT', '/recargaonline/api'); //Dev
//define('ROOT', '/api'); //Prod
function siteURL()
{
    $protocol = 'http://';
    $domainName = $_SERVER['HTTP_HOST'].'/';
    return $protocol.$domainName;
}
define( 'SITE_URL', siteURL() );