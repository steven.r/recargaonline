BEGIN;
CREATE TABLE profile (
	id INTEGER auto_increment
	,name VARCHAR(255)
	,PRIMARY KEY (id)
	);

CREATE TABLE module (
	id INTEGER auto_increment
	,name VARCHAR(255)
	,PRIMARY KEY (id)
	);

CREATE TABLE operator (
	id INTEGER auto_increment
	,name VARCHAR(255)
	,PRIMARY KEY (id)
	);

CREATE TABLE plan (
	id INTEGER auto_increment
	,NAME VARCHAR(255)
	,operator_id INTEGER
	,PRIMARY KEY (id)
	,FOREIGN KEY (operator_id) REFERENCES operator(id) ON DELETE CASCADE
	);

CREATE TABLE user (
	id INTEGER auto_increment
	,profile_id INTEGER
	,email VARCHAR(255) UNIQUE
	,password VARCHAR(255)
        ,api_key VARCHAR(255)
	,active boolean DEFAULT true
	,created_date DATETIME DEFAULT CURRENT_TIMESTAMP
	,PRIMARY KEY (id)
	,FOREIGN KEY (profile_id) REFERENCES PROFILE (id) ON DELETE SET NULL
	);

CREATE TABLE api_key (
    id INTEGER auto_increment
    ,user_id INTEGER
    ,apikey VARCHAR(255)
    ,expireddate DATETIME DEFAULT CURRENT_TIMESTAMP
    ,PRIMARY KEY (id)
    ,FOREIGN KEY (user_id) REFERENCES user(id) ON DELETE CASCADE
);

CREATE TABLE phonenumber (
	id INTEGER auto_increment
	,number VARCHAR(255)
	,operator_id INTEGER
	,plan_id INTEGER
	,user_id INTEGER
	,balance NUMERIC(15, 2)
	,PRIMARY KEY (id)
	,FOREIGN KEY (operator_id) REFERENCES operator(id) ON DELETE SET NULL
	,FOREIGN KEY (plan_id) REFERENCES PLAN (id) ON DELETE SET NULL
	,FOREIGN KEY (user_id) REFERENCES user(id) ON DELETE CASCADE
	);
	
CREATE TABLE plandetail (
	id INTEGER auto_increment
	,plan_id INTEGER
	,destination_operator_id INTEGER
	,cost NUMERIC(15,2)
	,PRIMARY KEY(id)
	,FOREIGN KEY (plan_id) REFERENCES plan(id) ON DELETE CASCADE
	,FOREIGN KEY (destination_operator_id) REFERENCES operator(id) ON DELETE SET NULL
	);

CREATE TABLE recharge (
		id INTEGER auto_increment
		,phonenumber_id INTEGER
		#,plan_id INTEGER
		,balance NUMERIC(15,2)
		,PRIMARY KEY (id)
		,FOREIGN KEY (phonenumber_id) REFERENCES phonenumber(id) ON DELETE CASCADE
		#,FOREIGN KEY (plan_id) REFERENCES plan(id)
		);
		
CREATE TABLE consumption (
		id INTEGER auto_increment
		,origin_phonenumber_id INTEGER 
		,destiny_phonenumber_id INTEGER	
                ,operator_id INTEGER	
		,call_duration TIME
		,balance_spent NUMERIC(15,2)
		,PRIMARY KEY (id)
		,FOREIGN KEY (origin_phonenumber_id) REFERENCES phonenumber(id) ON DELETE CASCADE
		#,FOREIGN KEY (destiny_phonenumber_id) REFERENCES phonenumber(id) ON DELETE SET NULL
                ,FOREIGN KEY (operator_id) REFERENCES operator(id) ON DELETE SET NULL
		#,FOREIGN KEY (plan_id) REFERENCES plan(id)
		);
CREATE TABLE permissionmodule (
	id INTEGER auto_increment
	,module_id INTEGER
	,profile_id INTEGER
	,create_p BOOLEAN DEFAULT false
	,read_p BOOLEAN DEFAULT false
	,update_p BOOLEAN DEFAULT false
	,delete_p BOOLEAN DEFAULT false
	,PRIMARY KEY (id)
	,FOREIGN KEY (module_id) REFERENCES module(id) ON DELETE CASCADE
	,FOREIGN KEY (profile_id) REFERENCES profile(id) ON DELETE CASCADE
	);		
	
COMMIT;