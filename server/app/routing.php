<?php

namespace recargaonline;

include_once __DIR__ . '/config/app.php';
include_once 'vendor/Slim/Slim.php';
foreach (glob(__DIR__ . '/controllers/*.php') as $filename) {
    include_once $filename;
}

\Slim\Slim::registerAutoloader();

class Route {

    var $app;

    public function __construct() {
        $this->app = new \Slim\Slim();
        $this->get();
        $this->register();
        $this->login();
        $this->logout();
        $this->getUser();
        $this->doRecharge();
        $this->getRecharges();
        $this->registerPhoneNumber();
        $this->getPhoneNumbers();
        $this->registerOperator();
        $this->getOperators();
        $this->registerPlan();
        $this->getPlans();
        $this->getBalancePhonenumber();
        $this->registerConsuption();
        $this->getComsuptions();
        $this->getPlanDetail();
        $this->app->run();
    }

    function authenticate(\Slim\Route $route = null) {

        $response = array();
        $isValid = null;
        $key = filter_input(INPUT_COOKIE, 'UUID');
        $userController = new UserController();
        if (isset($key) && $key) {
            $apiKey = new ApiKey();
            $isValid = $apiKey->getByColumn(array('apikey' => $key));
            $expiredKey = false;

            if ($isValid) {
                //validating api key      
                $expiredDateKey = new \DateTime($isValid[0]['expireddate']);
                $userTimezone = new \DateTimeZone(date_default_timezone_get());
                $expiredDate = $expiredDateKey->format('Y-m-d H:i:s');
                //Se traslada la hora actual del servidor de la app a la de la base de datos
                //para comparar las fechas de manera correcta (para usuarios fuera del timezon del server)
                $currentDate = new \DateTime(date("Y-m-d H:i:s"));
                $currentDate->setTimezone($userTimezone);
                $currentDate = $currentDate->format('Y-m-d H:i:s');
                $expiredKey = (strtotime($expiredDate) < strtotime($currentDate)) ? true : false;

                if (!$isValid || $expiredKey) {
                    $userController->deleteCookies();
                    $response = array('error' => true, 'message' => 'Acceso denegado. API Key inválida.');
                    $this->echoResponse(401, $response);
                    $this->app->stop();
                    return true;
                }
                //Intervalo para determinar cuando renovar la key, 5M = 5 min
                $expiredDateSub = $expiredDateKey->sub(new \DateInterval('PT5M'))->format('Y-m-d H:i:s');

                if (strtotime($expiredDateSub) <= strtotime($currentDate) && strtotime($expiredDate) > strtotime($currentDate)) {

                    //Definir estrategia para borrar llaves caducadas
                    //o cuando la key se cambie, no de error debido
                    //a que busca informacion basada en esa key
                    //1-Amarrar key nueva con key anterior para cuando cierre sesion
                    //borre en cascada
                    $userController->regenerateApiKey($isValid[0]);
                }
            } else {
                // api key is missing in header
                $userController->deleteCookies();
                $response = array('error' => true, 'message' => 'Api Key inválida.');
                $this->echoResponse(400, $response);
                try {
                    $this->app->stop();
                    return true;
                } catch (Exception $e) {
                    echo 'Excepción capturada: ', $e->getMessage(), "\n";
                }
            }
        } else {
            // api key is missing in header
            $userController->deleteCookies();
            $response = array('error' => true, 'message' => 'Api Key sin asignar.');
            $this->echoResponse(400, $response);
            try {
                $this->app->stop();
                return true;
            } catch (Exception $e) {
                echo 'Excepción capturada: ', $e->getMessage(), "\n";
            }
        }
    }

    function echoResponse($status_code, $response) {
        $this->app->response->headers->set('Content-Type', 'application/json');
        // Http response code
        $status = (isset($response['status']))? $response['status']: $status_code;
        $this->app->status($status);
        unset($response['status']);
        unset($response['error']);
        echo json_encode($response);
    }

    function get() {
        $this->app->get(ROOT . '/foo', array($this, 'authenticate'), function() {
            $this->echoResponse(200, 'OOP Works');
//            $this->app->response->headers->set('Content-Type', 'application/json');
//            $this->app->status(200);
//            echo json_encode('Works!');
        });
    }

    /**
     * User Registration
     * url - /register
     * method - POST
     * params - name, email, password
     */
    function register() {
        $this->app->post(ROOT . '/users', function() {
            $data = (array) json_decode($this->app->request->getBody());
            $userController = new UserController();
            $response = $userController->create($data);
            $status = (!$response['error']) ? 201 : 200;
            $this->echoResponse($status, $response);
        });
    }

    function login() {
        $this->app->post(ROOT . '/login', function() {
            $data = (array) json_decode($this->app->request->getBody());
            $userController = new UserController();
            $response = $userController->checkLogin($data);
            $status = 200; //(!$response['error']) ? 201 : 200;
            $this->echoResponse($status, $response);
        });
    }

    function logout() {
        $this->app->post(ROOT . '/logout', function() {
            $apiKey = filter_input(INPUT_COOKIE, 'UUID');
            $userController = new UserController();
            $response = $userController->logout($apiKey);
            $status = 200; //(!$response['error']) ? 201 : 200;
            $this->echoResponse($status, $response);
        });
    }

    function getUser() {
        $this->app->get(ROOT . '/users', array($this, 'authenticate'), function() {
            $key = filter_input(INPUT_COOKIE, 'UUID');            
            $userController = new UserController();
            $response = $userController->get($key);
            $status = 200; //(!$response['error']) ? 201 : 200;
            $this->echoResponse($status, $response);
        });
    }
    
    public function doRecharge() {
        $this->app->post(ROOT . '/recharges', array($this, 'authenticate'), function() {
            $data = (array) json_decode($this->app->request->getBody());
            $rechargeController = new RechargeController();
            $response = $rechargeController->doRecharge($data);
            $status = ($response['error'])? 400: 200;
            $this->echoResponse($status, $response);
        });
    }
    
    function getRecharges() {
        $this->app->get(ROOT . '/recharges', array($this, 'authenticate'), function() {
            $rechargeController = new RechargeController();
            $response = $rechargeController->getRecharges();
            $status = 200;
            $this->echoResponse($status, $response);
        });
    }
    
    function registerPhoneNumber() {
        $this->app->post(ROOT . '/phonenumbers', array($this, 'authenticate'), function() {
            $data = (array) json_decode($this->app->request->getBody());
            $phoneNumberController = new PhoneNumberController();
            $response = $phoneNumberController->registerPhoneNumber($data);
            $status = 200;
            $this->echoResponse($status, $response);
        });
    }
    
    function getPhoneNumbers() {
        $this->app->get(ROOT . '/phonenumbers', array($this, 'authenticate'), function() {
            $phoneNumberController = new PhoneNumberController();
            $response = $phoneNumberController->getPhoneNumbers();
            $status = 200;
            $this->echoResponse($status, $response);
        });
    }
    
    function registerOperator() {
        $this->app->post(ROOT . '/operators', array($this, 'authenticate'), function() {
            $data = (array) json_decode($this->app->request->getBody());
            $operatorController = new OperatorController();
            $response = $operatorController->registerOperator($data);
            $status = 200;
            $this->echoResponse($status, $response);
        });
    }
    
    function getOperators() {
        $this->app->get(ROOT . '/operators', array($this, 'authenticate'), function() {
            $operatorController = new OperatorController();
            $response = $operatorController->getOperators();
            $status = 200;
            $this->echoResponse($status, $response);
        });
    }
    
    function registerPlan() {
        $this->app->post(ROOT . '/plans', array($this, 'authenticate'), function() {
            $data = (array) json_decode($this->app->request->getBody());
            $planController = new PlanController();
            $response = $planController->registerPlan($data);
            $status = 200;
            $this->echoResponse($status, $response);
        });
    }
    
    function getPlans() {
        $this->app->get(ROOT . '/plans(/:id)', array($this, 'authenticate'), function($id = '') {
            $planController = new PlanController();
            $response = $planController->getPlans($id);
            $status = 200;
            $this->echoResponse($status, $response);
        });
    }
    
    function getBalancePhonenumber() {
        $this->app->get(ROOT . '/phonenumbers/:number', array($this, 'authenticate'), function($number = '') {
            $phoneNumberController = new PhonenumberController();
            $response = $phoneNumberController->balancePhoneNumber($number);
            $status = 200;
            $this->echoResponse($status, $response);
        });
    }
    
    function registerConsuption() {
        $this->app->post(ROOT . '/comsuptions', array($this, 'authenticate'), function() {
            $data = (array) json_decode($this->app->request->getBody());
            $comsuptionController = new ComsuptionController();
            $response = $comsuptionController->registerComsuption($data);
            $status = 200;
            $this->echoResponse($status, $response);
        });
    }
    
    function getComsuptions() {
        $this->app->get(ROOT . '/comsuptions', array($this, 'authenticate'), function() {
            $comsuptionController = new ComsuptionController();
            $response = $comsuptionController->getComsuptions();
            $status = 200;
            $this->echoResponse($status, $response);
        });
    }
    
    function getPlanDetail() {
        $this->app->get(ROOT . '/plans/:id/details', array($this, 'authenticate'), function($id = '') {
            $planController = new PlanController();
            $response = $planController->getPlanDetail($id);
            $status = 200;
            $this->echoResponse($status, $response);
        });
    }
    

}

$route = new Route();
