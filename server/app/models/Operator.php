<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace recargaonline;
include_once __DIR__ .'/Model.php';
/**
 * Description of Operator
 *
 * @author Diedrych
 */
class Operator extends Model {
    
    protected $table = 'operator';
    protected $columns = ['id', 'name', 'createddate'];
}
