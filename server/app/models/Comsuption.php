<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace recargaonline;
include_once __DIR__ .'/Model.php';
/**
 * Description of Comsuption
 *
 * @author Administrador
 */
class Comsuption extends Model {
    
    protected $table = 'consumption';
    protected $columns = ['id', 'origin_phonenumber_id', 'destiny_phonenumber_id'
        ,'operator_id','call_duration' ,'balance_spent','createddate'];
    
    function getAll() {
        $sql = "SELECT \"$this->table\".*, PN1.number AS origin_phonenumber, PN2.number AS destiny_phonenumber "
                ."FROM \"$this->table\" "
                ."LEFT JOIN \"phonenumber\" PN1 on (\"$this->table\".origin_phonenumber_id = PN1.id)"
                ."LEFT JOIN \"phonenumber\" PN2 on (\"$this->table\".destiny_phonenumber_id = PN2.id) ";
        $stmt = $this->dbconn->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll();
    }
    
    
}
