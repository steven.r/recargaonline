<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace recargaonline;
include_once __DIR__ .'/Model.php';
/**
 * Description of PhoneNumber
 *
 * @author Usuario
 */
class PhoneNumber extends Model{
    
    protected $table = 'phonenumber';
    protected $columns = ['id', 'number', 'operator_id', 'plan_id','user_id', 'balance'];
    
    public function getAll() {        
        $sql = "SELECT \"$this->table\".*, \"operator\".name AS operator_name, \"plan\".name AS plan_name  "
                . "FROM \"$this->table\" "
                . "LEFT JOIN \"operator\" ON (\"$this->table\".operator_id = \"operator\".id) "
                . "LEFT JOIN \"plan\" ON (\"$this->table\".plan_id = \"plan\".id);";
        $stmt = $this->dbconn->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll();
    }
    
}
