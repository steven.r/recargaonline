<?php

/*
 * Tomado de: https://uno-de-piera.com/el-patron-singleton-en-php/
 */

namespace recargaonline;
include_once __DIR__ .'/../config/database.php';
/**
 * @author https://uno-de-piera.com/
 */
class DBConnection {

    // Contenedor de la instancia del singleton
    private static $instance;
    private $db;

    private function __construct() { 
        global $dbconn;
        $this->db = new \PDO("$dbconn[rdbms]:host=$dbconn[host];dbname=$dbconn[dbname]", 
                $dbconn['user'], $dbconn['password']);
    }

    public static function singleton() {
        if (!isset(self::$instancia)) {
            $myClass = __CLASS__;
            self::$instance = new $myClass;
        }
        return self::$instance;
    }
    
    function getDBConn()
    {
        return $this->db;
    }

    public function __clone() {
        trigger_error('Clone this object is restricted', E_USER_ERROR);
    }

}