<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace recargaonline;
include_once __DIR__ .'/Model.php';
/**
 * Description of Plan
 *
 * @author Diedrych
 */
class Plan extends Model {
    
    protected $table = 'plan';
    protected $columns = ['id', 'name', 'operator_id', 'createddate'];
    
    
    function registerPlanDetail($values) {
        
        
        $query = 'INSERT INTO "plandetail"(plan_id, operator_id, destination_operator_id, cost) VALUES '; //Prequery
        $qPart = array_fill(0, count($values), "(?, ?, ?, ?)");
        $query .= implode(",", $qPart);
        $stmt = $this->dbconn->prepare($query);
        $i = 1;
        foreach ($values as $item) { //bind the values one by one
            $stmt->bindValue($i++, $item['plan_id']);
            $stmt->bindValue($i++, $item['operator_id']);
            $stmt->bindValue($i++, $item['destination_operator_id']);
            $stmt->bindValue($i++, $item['cost']);
        }        
        return ($stmt->execute()) ? true : false;
    }
    
    function getAll() {
        $sql = "SELECT \"$this->table\".*, \"operator\".name AS operator_name  FROM \"$this->table\" JOIN \"operator\" ON (\"$this->table\".operator_id = \"operator\".id);";
        $stmt = $this->dbconn->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll();
    }
    
    function getPlanDetail($planId) {
        $sql = "SELECT \"operator\".id AS operator_id
                        ,\"operator\".name AS operator_name
                        ,\"$this->table\".id AS plan_id
                        ,\"$this->table\".name AS plan_name
                        ,\"plandetail\".destination_operator_id AS destination_operator_id
                        ,\"plandetail\".cost AS cost
                        ,\"operator_destionation\".name AS destination_operator_name
                FROM \"operator\"
                LEFT JOIN \"$this->table\" ON (\"operator\".id = \"$this->table\".operator_id)
                LEFT JOIN \"plandetail\" ON (\"$this->table\".id = \"plandetail\".plan_id)
                LEFT JOIN \"operator\" AS \"operator_destionation\" ON (\"plandetail\".destination_operator_id = \"operator_destionation\".id)
                WHERE \"$this->table\".id = :id ;";        
        $stmt = $this->dbconn->prepare($sql);
        $stmt->bindValue(':id', $planId, \PDO::PARAM_INT);
        $stmt->execute();
        return $stmt->fetchAll();
    }
    
    function getCostOperatorToOperator($values) {
        $originOperator = $values['originOperator'];
        $destinyOperator = $values['destinyOperator'];
        
        $sql = "SELECT \"operator\".id AS operator_id
                        ,\"operator\".name AS operator_name
                        ,\"$this->table\".id AS plan_id
                        ,\"$this->table\".name AS plan_name
                        ,\"plandetail\".destination_operator_id AS destination_operator_id
                        ,\"plandetail\".cost AS cost
                        ,\"operator_destionation\".name AS destination_operator_name
                FROM \"operator\"
                LEFT JOIN \"$this->table\" ON (\"operator\".id = \"$this->table\".operator_id)
                LEFT JOIN \"plandetail\" ON (\"$this->table\".id = \"plandetail\".plan_id)
                LEFT JOIN \"operator\" AS \"operator_destionation\" ON (\"plandetail\".destination_operator_id = \"operator_destionation\".id)
                WHERE \"$this->table\".operator_id = :operator_id AND destination_operator_id = :destination_operator_id;";        
        $stmt = $this->dbconn->prepare($sql);
        $stmt->bindValue(':operator_id', $originOperator, \PDO::PARAM_INT);
        $stmt->bindValue(':destination_operator_id', $destinyOperator, \PDO::PARAM_INT);
        $stmt->execute();
        return $stmt->fetchAll();
    }
    
}
