<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace recargaonline;
include_once __DIR__ .'/DBConnection.php';
/**
 * Description of Model
 *
 * @author Usuario
 */
class Model {

    //put your code here
    protected $table = '';
    protected $columns = [];
    public $dbconn;

    public function __construct() {
        $this->dbconn = DBConnection::singleton()->getDBConn();
    }

    public function create($values) {
        $columns = array();
        foreach ($values as $key => $value) {
            $columns[] = $key;
        }
        $columns = implode(',', $columns);
        $preparedColumns = ':' . str_replace(',', ',:', $columns);
        $sql = "INSERT INTO \"$this->table\"($columns) VALUES ($preparedColumns);";
        $stmt = $this->dbconn->prepare($sql);
        foreach ($values as $column => $value) {
            $stmt->bindParam(":$column", $value, \PDO::PARAM_STR);
        }           
        return ($stmt->execute($values))? true : false;
    }
    
    public function get($id) {        
        $sql = "SELECT * FROM \"$this->table\" WHERE id = :id;";
        $stmt = $this->dbconn->prepare($sql);
        $stmt->bindValue(':id', $id, \PDO::PARAM_INT);
        $stmt->execute();
        return $stmt->fetchAll();        
    }
    
    public function getAll() {        
        $sql = "SELECT * FROM \"$this->table\";";
        $stmt = $this->dbconn->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll();
    }
    
    public function getByColumn($values, $connector = 'AND') {
        $whereParameters = '';
        foreach ($values as $key => $value) {
            $whereParameters.= " $key = :$key $connector";
        }
        $pos = strrpos($whereParameters, $connector);

        if ($pos !== false) {
            $whereParameters = substr_replace($whereParameters, '', $pos, strlen($connector));
        }
        $sql = "SELECT * FROM \"$this->table\" WHERE $whereParameters;";        
        $stmt = $this->dbconn->prepare($sql);
        foreach ($values as $column => $value) {
            $type = (is_numeric($value))? \PDO::PARAM_INT: \PDO::PARAM_STR;
            $c = $value;
            $stmt->bindValue(":$column", $c, $type);
            //$stmt->bindParam(":$column", $value);
        }   
        $stmt->execute();
//        var_dump($values);
//        var_dump($stmt);
        return $stmt->fetchAll(); 
    }
    
    public function update($id, $values) {
        $sql = "UPDATE \"$this->table\" SET ";
        //$set = implode('', $pieces)
        
        foreach ($values as $key => $value) {
            $sql .= " $key = :$key,";
        }
        $pos = strrpos($sql, ',');

        if ($pos !== false) {
            $sql = substr_replace($sql, '', $pos, strlen(','));
        }
        
        $sql .= " WHERE id = :id;";        
        $stmt = $this->dbconn->prepare($sql);
        $stmt->bindValue(':id', $id, \PDO::PARAM_INT);
        foreach ($values as $key => $value) {
            $stmt->bindValue(":$key", $value, \PDO::PARAM_STR);
        }
        return $stmt->execute();        
    }
    
    public function delete($id) {
        $sql = "DELETE FROM \"$this->table\" WHERE id = :id;";
        $stmt = $this->dbconn->prepare($sql);
        $stmt->bindValue(':id', $id, \PDO::PARAM_INT);
        $stmt->execute();
        return $stmt->execute();
    }
    
    public function getAllJoin($tables = array()) {
        $foo = '';
        foreach ($tables as $table) {
            $foo .= " JOIN \"$table\" ON (\"$this->table\".".$table."_id = \"$table\".id) ";
        }        
        $sql = "SELECT $this->table.id AS ".$this->table."_id, * FROM \"$this->table\" $foo;";
        $stmt = $this->dbconn->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll();
    }
}