<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace recargaonline;
include_once __DIR__ .'/Model.php';
/**
 * Description of Recharge
 *
 * @author Usuario
 */
class Recharge extends Model {
    
    protected $table = 'recharge';
    protected $columns = ['id', 'phonenumber_id', 'balance'];
    
    function getRecord() {
        $sql = "SELECT $this->table.id
                       ,$this->table.balance
                       ,$this->table.createddate
                       ,phonenumber.number
                FROM \"$this->table\"
                JOIN phonenumber ON ($this->table.phonenumber_id = phonenumber.id);";
        $stmt = $this->dbconn->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll(); 
    }
}