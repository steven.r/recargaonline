<?php
namespace recargaonline;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

include_once __DIR__ .'/Model.php';
/**
 * Description of User
 *
 * @author Usuario
 */
class User extends Model {

    protected $table = 'user';
    protected $columns = ['id', 'email', 'password', 'active', 'created_date', 
        'profile_id'];
    
    public function login($email, $password) {
        $sql = "SELECT * FROM $this->table WHERE email = :email AND password = :password;";
        $stmt = $this->dbconn->prepare($sql);
        $stmt->bindValue(':email', $email, \PDO::PARAM_STR);
        $stmt->bindValue(':password', $password, \PDO::PARAM_STR);
        $stmt->execute();
        return $stmt->fetchAll();  
    }
    
    public function logout() {
        
    }
}