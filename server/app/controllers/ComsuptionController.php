<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace recargaonline;
include_once __DIR__ . '/../models/Comsuption.php';
include_once __DIR__ . '/../models/PhoneNumber.php';
include_once __DIR__ . '/../models/Plan.php';
include_once __DIR__ . '/Controller.php';
include_once __DIR__ . '/PhoneNumberController.php';
include_once 'Controller.php';
/**
 * Description of ComsuptionController
 *
 * @author Administrador
 */
class ComsuptionController extends Controller {
    
    var $comsuptionModel;
    var $phoneNumberModel;
    var $planModel;

    public function __construct() {
        $this->comsuptionModel = new Comsuption();
        $this->phoneNumberModel = new PhoneNumber();
        $this->planModel = new Plan();
    }
    
    public function registerComsuption($values) {
        $requiredFields = array('originPhoneNumber','destinyPhoneNumber','minutes','seconds');
        $response = $this->verifyRequiredFields($requiredFields, $values);
        if ($response['error'] === true) {
            $response['status'] = 400;
            return $response;
        }        
        
        $validateOriginPhoneNumber = preg_match('/^\d+$/',$values['originPhoneNumber']);
        $validateDestinyPhoneNumber = preg_match('/^\d+$/',$values['destinyPhoneNumber']);
        $validateMinutes = preg_match('/^\d+$/',$values['minutes'])  && $values['minutes'] >= 0;  
        $validateSeconds = preg_match('/^\d+$/',$values['seconds']) && $values['seconds'] >= 0 && $values['seconds'] < 60 ;  
                
        if ($validateOriginPhoneNumber !== 1 || $validateDestinyPhoneNumber !== 1 || !$validateMinutes || !$validateSeconds) {
            $response['message'] = 'Los números o tiempo de la llamada ingresados contienen errores. Por favor, revise.';
            $response['error'] = true;
            $response['status'] = 400;
            return $response;
        }
        $phoneNumberOriginRow = $this->phoneNumberModel->getByColumn(array('number' => (string) $values['originPhoneNumber']));
        $phoneNumberDestinyRow = $this->phoneNumberModel->getByColumn(array('number' => (string) $values['destinyPhoneNumber']));
        
        if (!$phoneNumberOriginRow) {
            $response['message'] = 'El número de origen no está registrado.';
            $response['status'] = 400;
            $response['error'] = true;
            return $response; 
        } else if (!$phoneNumberDestinyRow) {
            $response['message'] = 'El número de destino no está registrado.';
            $response['status'] = 400;
            $response['error'] = true;
            return $response; 
        }
        $operators = array('originOperator' => $phoneNumberOriginRow[0]['operator_id']
                ,'destinyOperator' =>$phoneNumberDestinyRow[0]['operator_id']);
        $planInformation =  $this->planModel->getCostOperatorToOperator($operators);
        
        if (!$planInformation) {
            $response['message'] = 'El plan del número de origen no tiene un costo asociado al operador del número destino.';
            $response['error'] = true;
            $response['status'] = 400;
            return $response;
        }
        $spentBalance = ($values['minutes'] * 60 + $values['seconds']) * $planInformation[0]['cost'];
        
        $hours = floor($values['minutes']/60);
        $minutes = $values['minutes'] - $hours * 60;
        $seconds = $values['seconds'];
        
        $seconds = ($values['seconds'] > 9)? $values['seconds'] : "0$values[seconds]";
        $minutes = ($minutes - $hours * 60 > 9)? $minutes : "0$minutes";
        $hours = ($hours > 9)? $hours : "0$hours";
        
        
        $durationCall = "$hours:$minutes:$seconds";
        $data = array('origin_phonenumber_id' => $phoneNumberOriginRow[0]['id']
            ,'destiny_phonenumber_id' => $phoneNumberDestinyRow[0]['id']
            ,'call_duration' => $durationCall
            ,'balance_spent' => $spentBalance);
        if (!$this->comsuptionModel->create($data) ) {
            $response['message'] = 'No se ha podido registrar el consumo. Por favor, contacte el administrador.';
            $response['error'] = true;
            $response['status'] = 400;
            return $response;
        } else {
            $rechargeController = new RechargeController(); 
            $response = $rechargeController->updateBalancePhoneNumber($phoneNumberOriginRow, -1 *$spentBalance);
            if ($response['error'] === true) {
                $response['message'] = 'Se ha registrado el consumo. Pero hubo un problema en descontar el saldo.';
                $response['status'] = 400;
            } else {
                $response['message'] = 'Se ha registrado el consumo.';
                $response['status'] = 201;
                $response['error'] = true;
            }
        }
        
        return $response;     
    }
    
    function getComsuptions() {
        $response = $this->comsuptionModel->getAll();
        $response['status'] = ($response)? 200 : 400;        
        return $response;
    }
    
}
