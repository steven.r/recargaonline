<?php
namespace recargaonline;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//include_once realpath(dirname(__FILE__).'models/User.php';
include_once __DIR__ .'/../models/User.php';
include_once __DIR__.'/../models/ApiKey.php';
include_once __DIR__.'/../vendor/PassHash.php';
include_once __DIR__ . '/Controller.php';
include_once 'Controller.php';

/**
 * Description of UserController
 *
 * @author Usuario
 */
class UserController extends Controller{
    
    var $userModel;
    
    public function __construct() {
        $this->userModel = new User();
    }
    
    public function get($key) {
        $apiKey = new ApiKey();
        $isValid = $apiKey->getByColumn(array('apikey' => $key));
        if (!$isValid) {
            $response['error'] = true;
            $response['message'] = 'No se encuentra un usuario asociado a la Key.';
            $response['status'] = 401;
        }
        $id = $isValid[0]['user_id'];
        $response = null;
        $user = $this->userModel->get($id);
        if (!$user) {
            $response['error'] = false;
            $response['message'] = 'El usuario no existe.';
            $response['status'] = 200;
        } else {
            $response = $user[0];
            $response['error'] = false;
            $response['status'] = 200;
        }
        return $response;
    }
    
    public function create($values) {
        $response = $this->verifyRequiredFields(array('email', 'password'), $values);
        if ($response['error'] === true) {
           $response['status'] = 400;
           return $response;
        }
        
        if (!filter_var($values['email'], FILTER_VALIDATE_EMAIL)) {
            $response['error'] = true;
            $response['message'] = 'El email es inválido';
            $response['status'] = 400;
            return $response;
        }
        
        $response = array();
        if ($this->userModel->getByColumn(array('email' => $values['email']))) {
            $response['error'] = true;
            $response['message'] = 'El correo ya está registrado.';
        } else {
            $fields['password'] = PassHash::hash($values['password']);
            $fields['email'] = $values['email'];
            //$values['api_key'] = $this->generateApiKey();
            if ($this->userModel->create($fields)) {
                $response['error'] = false;
                $response['message'] = 'El registro fue exitoso.';
                $response['status'] = 201;
            } else {
                $response['error'] = true;
                $response['message'] = 'El registro ha fallado.';
                $response['status'] = 400;
            }
        }
        return $response;
    }

    /**
     * Checking user login
     * @param String $email User login email id
     * @param String $password User login password
     * @return boolean User login status success/fail
     */
    public function checkLogin($values) {
        $email = $values['email'];
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $response['error'] = true;
            $response['message'] = 'El email es inválido';
            $response['status'] = 400;
            return $response;
        }

        $response = $this->verifyRequiredFields(array('email', 'password'), $values);
        if ($response['error'] === true) {
            $response['status'] = 400;
            return $response;
        }

        $response = array();
        // fetching user by email
        $user = $this->userModel->getByColumn(array('email' => $values['email']));
        if (!$user) {
            $response['error'] = true;
            $response['message'] = 'El correo no está registrado.';
            $response['status'] = 401;
            return $response;
        }
        $passwordHash = $user[0]['password'];
        if (PassHash::check_password($passwordHash, $values['password'])) {
            //Delete cookies to web app
            $this->deleteCookies();
            // User password is correct            
            $this->generateApiKey($user[0]['id']);
            $response['error'] = false;
            $response['message'] = 'El inicio de sesión fue exitoso.';
            $response['status'] = 200;
        } else {
            // user password is incorrect
            $response['error'] = true;
            $response['message'] = 'La contraseña o correo es incorrecto.';
            $response['status'] = 401;
        }
        return $response;        
    }
    
    /*
     * Generate API Key
     */    
    function setApiKey($time = null) {
        $time = ($time === null)? time() + 3600 * 24: $time;
        $httpUser = filter_input(INPUT_SERVER, 'HTTP_USER_AGENT');
        $httpAccept = filter_input(INPUT_SERVER, 'HTTP_ACCEPT');
        $remoteAddress = filter_input(INPUT_SERVER, 'REMOTE_ADDR');
        $server = filter_input(INPUT_SERVER, 'SERVER_NAME');
        $key = $httpUser . $httpAccept . $remoteAddress . rand(0, PHP_INT_MAX);
        $key = hash('sha256', $key);        
        setcookie('UUID', $key, strtotime($time), '/', $server, false, false);
        return $key;
    }
    
    function generateApiKey($userId) {        
        $time = date("Y-m-d H:i:s", strtotime('+1 hours'));
        $key = $this->setApiKey($time);
        $apiKey = new ApiKey();
        $values = array('user_id' => $userId ,'apikey' => $key,'expireddate' => $time);
        $apiKey->create($values);
        //return $key;
    }
    
    function regenerateApiKey($apiKeyValues) {
        $time = date("Y-m-d H:i:s", strtotime('+1 hours'));        
        $key = $this->setApiKey();
        $apiKey = new ApiKey();
        $values = array('apikey' => $key,'expireddate' => $time);
        //$apiKey->update($apiKeyValues['id'], $values);
        //Definir estrategia para borrar llaves caducadas
        //1-Amarrar key nueva con key anterior para cuando cierre sesion
        //borre en cascada
        $apiKey->create(array('user_id' => $apiKeyValues['user_id'],'apikey' => $key,'expireddate' => $time));
    }
    
    function logout($key) {
        $apiKey = new ApiKey();
        $keyId = $apiKey->getByColumn(array('apikey' => $key));
        if (count($keyId) <= 0) {
            $response['error'] = true;
            $response['message'] = 'Sesión NO fue cerrada correctamente.';
            $response['status'] = 400;
            return $response;
        }
        $id = $keyId[0]['id'];
        $apiKey->delete($id);
        $this->deleteCookies();
        return array('error' => false, 'message' => 'Sesión cerrada correctamente.');
    }
    
    function deleteCookies() {
        // unset cookies
        //setcookie('null', '', time()-1000);
        $server = filter_input(INPUT_SERVER, 'SERVER_NAME');
        if (isset($_SERVER['HTTP_COOKIE'])) {
            $cookies = explode(';', $_SERVER['HTTP_COOKIE']);
            foreach($cookies as $cookie) {
                $parts = explode('=', $cookie);
                $name = trim($parts[0]);
                setcookie($name, '', time()-1000);
                setcookie($name, '', time()-1000, '/');    
                setcookie($name, '', time()-1000, '/', $server, false, false);
            }
        }
    }
}