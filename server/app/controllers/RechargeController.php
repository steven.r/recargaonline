<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace recargaonline;

include_once __DIR__ . '/../models/PhoneNumber.php';
include_once __DIR__ . '/../models/Recharge.php';
include_once __DIR__ .'/Controller.php';
include_once 'Controller.php';

/**
 * Description of PhoneNumberController
 *
 * @author Usuario
 */
class RechargeController extends Controller {

    var $rechargeModel;
    var $phoneNumberModel;
    

    public function __construct() {
        $this->rechargeModel = new Recharge();
        $this->phoneNumberModel = new PhoneNumber();        
    }

    public function doRecharge($values) {
        $requiredFields = array('phoneNumber', 'balance');
        $response = $this->verifyRequiredFields($requiredFields, $values);
        if ($response['error'] === true) {
            $response['status'] = 400; 
            return $response;
        }        
        
        $validatePhoneNumber = preg_match('/^\d+$/',$values['phoneNumber']); //is_int($values['phoneNumber']); //filter_var($values['phoneNumber'], FILTER_VALIDATE_INT);
        $validateBalance = filter_var($values['balance'], FILTER_VALIDATE_FLOAT) && $values['balance'] >= 0;        
                
        if ($validatePhoneNumber !== 1 || !$validateBalance) {
            $response['message'] = 'El número o valor de la recarga ingresado contiene errores. Por favor, revise.';
            $response['error'] = true;
            $response['status'] = 400;
            return $response;
        }
        $phoneNumberRow = $this->phoneNumberModel->getByColumn(array('number' => (string) $values['phoneNumber']));
        if ($phoneNumberRow) {
            $create = array('phonenumber_id' => $phoneNumberRow[0]['id'], 'balance' => $values['balance']);
            $response = $this->updateBalancePhoneNumber($phoneNumberRow, $values['balance']);
            //$response['status'] = 200;
            if ($response['error'] === true) {
                $response['status'] = 400;
                return $response;
            }
            if (!$this->rechargeModel->create($create)) {
                $response['message'] .= ' Pero ha ocurrido un error con la recarga. Por favor, avise esto al administrador del sistema.';
                $response['error'] = true;
                $response['status'] = 200;
            }
        } else {
            $response['message'] = 'El número no está registrado. Por favor, registrelo primero.';
            $response['error'] = true;
            $response['status'] = 400;
        }
        return $response;        
    }
    
    function updateBalancePhoneNumber($values, $balance = 0) {
        $balance += $values[0]['balance']; 
        if ($this->phoneNumberModel->
                update($values[0]['id'], array('balance' => $balance))) {
            $response['message'] = 'La recarga fue exitosa.';
            $response['error'] = false;
            $response['status'] = 201;
        } else {
            $response['message'] = 'Ha ocurrido un error con la recarga. Por favor, verifique el número y valor e intente nuevamente.';
            $response['error'] = true;
            $response['status'] = 400;
        }
        return $response;
    }
    
    function getRecharges() {
        $response = $this->rechargeModel->getRecord();
        $response['status'] = ($response)? 200 : 400;        
        return $response;
    }

}