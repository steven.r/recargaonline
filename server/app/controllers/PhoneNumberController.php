<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace recargaonline;

include_once __DIR__ . '/../models/PhoneNumber.php';
include_once __DIR__ . '/../models/Recharge.php';
include_once __DIR__ . '/../models/Plan.php';
include_once __DIR__ . '/Controller.php';
include_once 'Controller.php';

/**
 * Description of PhoneNumberController
 *
 * @author Usuario
 */
class PhoneNumberController extends Controller {

    var $phoneNumberModel;
    var $rechargeModel;
    var $planModel;

    public function __construct() {
        $this->phoneNumberModel = new PhoneNumber();
        $this->rechargeModel = new Recharge();
        $this->planModel = new Plan();
    }

    public function getPhoneNumberUser($user_id) {
        $response = null;
        $phonenumbers = $this->phoneNumberModel->getByColumn(array('user_id' => $user_id));
        if (!$phonenumbers) {
            $response['message'] = 'El usuario no existe.';
            $response['status'] = 200;
        } else {
            $response = $phonenumbers;
            $response['status'] = 200;
        }
        return $response;
    }

    public function registerPhoneNumber($values) {
        $requiredFields = array('phoneNumber', 'operator', 'plan');
        $data = array('number' => $values['phoneNumber']);
        $response = $this->verifyRequiredFields($requiredFields, $values);
        if ($response['error'] === true) {
            $response['status'] = 400;
            return $response;
        }

        $validatePhoneNumber = preg_match('/^\d+$/', $values['phoneNumber']);

        if ($validatePhoneNumber !== 1) {
            $response['message'] = 'El número ingresado contiene errores. Por favor, revise.';
            $response['error'] = true;
            $response['status'] = 400;
            return $response;
        } elseif ($this->phoneNumberModel->getByColumn($data)) {
            $response['message'] = 'El número ya se encuentra registrado.';
            $response['error'] = true;
            $response['status'] = 400;
            return $response;
        }
        
        $data = array('number' => $values['phoneNumber'],  'operator_id' => $values['operator'], 'plan_id' => $values['plan']);
        if (!$this->phoneNumberModel->create($data)) {
            $response['message'] = 'Ha ocurrido un error con el registro del número. Por favor, avise esto al administrador del sistema.';
            $response['error'] = true;
            $response['status'] = 400;
        } else {
            $response['message'] = 'El registro ha sido exitoso.';
            $response['error'] = false;
            $response['status'] = 201;
        }
        return $response;
    }
    
    function getPhoneNumbers() {
        $response = $this->phoneNumberModel->getAll();
        $response['status'] = ($response)? 200 : 400;        
        return $response;
    }
    
    function balancePhoneNumber($values) {
        $response = array();
        $phoneNumber = $this->phoneNumberModel->getByColumn(array('number' => $values));
        if (!$phoneNumber) {
            /*$response['message'] = 'El número no se encuentra registrado.';
            $response['error'] = false;*/
            $response['message'] = 'El número no se encuentra registrado.';
            $response['status'] = 400;
            return $response;
        }
        $planId = $phoneNumber[0]['plan_id'];
        $response = $this->planModel->getPlanDetail($planId);
        foreach ($response as &$cost) {
            $costSecond = ($cost['cost'] == 0)? 1: $cost['cost'];
            $cost['seconds'] = number_format((float) $phoneNumber[0]['balance']/$costSecond, 2, '.', '');
            $cost['number'] = $phoneNumber[0]['number'];
            $cost['phonenumber_id'] = $phoneNumber[0]['id'];
            $cost['balance'] = $phoneNumber[0]['balance'];
        }
        unset($cost);
        $response['status'] = 200;
        return $response;
    }

}