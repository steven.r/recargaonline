<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace recargaonline;

/**
 * Description of Controller
 *
 * @author Usuario
 */
class Controller {

    public function verifyRequiredFields($requiredFields, $inputFields) {
        $error = false;
        $errorFields = '';
        foreach ($requiredFields as $field) {
            if (isset($inputFields[$field]) && is_array($inputFields[$field]) && ($inputFields[$field] instanceof Traversable)) {
                
            } else if (!isset($inputFields[$field]) || (!is_array($inputFields[$field]) && strlen(trim($inputFields[$field])) <= 0 )) {
                $error = true;
                $errorFields .= $field . ', ';
            }
        }
        
        $response = array();
        $response["error"] = false;
        if ($error) {
            $response["error"] = true;
            $response["message"] = 'Los campos ' . substr($errorFields, 0, -2) . ' faltan.';
            return $response;
        }
        return $response;
    }

}
