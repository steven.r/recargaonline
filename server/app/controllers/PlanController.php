<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace recargaonline;

include_once __DIR__ . '/../models/Plan.php';
include_once __DIR__ . '/Controller.php';
include_once 'Controller.php';
/**
 * Description of PlanController
 *
 * @author Diedrych
 */
class PlanController extends Controller {
    
    var $planModel;

    public function __construct() {
        $this->planModel = new Plan();
    }
    
    public function registerPlan($values) {
        $requiredFields = array('planName', 'operator', 'costOperator');
        $data = array('name' => $values['planName'], 'operator_id' => $values['operator']);
        $response = $this->verifyRequiredFields($requiredFields, $values);
        if ($response['error'] === true) {
            $response['status'] = 400; 
            return $response;
        }

        $validateOperatorName = (strlen($values['planName']) > 0 && strlen(trim($values['planName'])) !== 0)? true: false;

        if (!$validateOperatorName) {
            $response['message'] = 'El nombre ingresado contiene errores. Por favor, revise.';
            $response['error'] = true;
            $response['status'] = 400; 
            return $response;
        } else if ($this->planModel->getByColumn($data)) {
            $response['message'] = 'Ya hay un plan para este operador con ese nombre registrado.';
            $response['error'] = true;
            $response['status'] = 400; 
            return $response;
        }        
        
        if (!$var = $this->planModel->create($data)) {
            $response['message'] = 'Ha ocurrido un error con el registro del plan. Por favor, avise esto al administrador del sistema.';
            $response['error'] = true;
            $response['status'] = 400; 
        } else {
            $value = array();
            foreach ($values['costOperator'] as $col) {
                $value[] = array(
                    'plan_id' => $this->planModel->dbconn->lastInsertId('plan_id_seq'),
                    'operator_id' => $values['operator'],
                    'destination_operator_id' => $col->operator_id,
                    'cost' =>  $col->cost
                );                
            }
            
            $response['message'] = 'El registro ha sido exitoso.';
            $response['error'] = false;
            $response['status'] = 201; 
            
            if (!$responsePlanDetail = $this->planModel->registerPlanDetail($value)) {
                $response['message'] .= ' Pero se ha presentado un problema al insertar los costos. Por favor, modifiquelos en la opción correspondiente.';
                $response['error'] = true;
                $response['status'] = 200; 
            }
            
        }
        return $response;
    }
    
    function getPlans($operatorId = '') {
        
        if ($operatorId === '') {
            $response = $this->planModel->getAll();              
        } else {
            $response = $this->planModel->getByColumn(array('operator_id' => $operatorId));
        }
        $response['status'] = ($response)? 200 : 400; 
        if ($response['status']  === 400) {
            $response['message'] = 'El operador no tiene planes asociados.';
        }
        return  $response;
    }
    
    function getPlanDetail($planId = '') {
        
        if ($planId === '') {
            //$response = $this->planModel->getPlanDetail($planId);   
            $response['message'] = 'El plan no está especificado.'; 
            $response['status'] = 400; 
        } else {
            $response = $this->planModel->getPlanDetail($planId);
        }
        $response['status'] = ($response)? 200 : 400; 
        if ($response['status']  === 400) {
            $response['message'] = 'El plan no tiene detalles asociados.';
        }
        return  $response;
    }   
    
}
