<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace recargaonline;

include_once __DIR__ . '/../models/Operator.php';
include_once __DIR__ . '/Controller.php';
include_once 'Controller.php';
/**
 * Description of OperatorController
 *
 * @author Diedrych
 */
class OperatorController extends Controller {
    
    var $operatorModel;

    public function __construct() {
        $this->operatorModel = new Operator();
    }
    
    public function registerOperator($values) {
        $requiredFields = array('operatorName');
        $data = array('name' => $values['operatorName']);
        $response = $this->verifyRequiredFields($requiredFields, $values);
        if ($response['error'] === true) {
            $response['status'] = 400;
            return $response;
        }

        $validateOperatorName = (strlen($values['operatorName']) > 0 && strlen(trim($values['operatorName'])) !== 0)? true: false;

        if (!$validateOperatorName) {
            $response['message'] = 'El nombre ingresado contiene errores. Por favor, revise.';
            $response['status'] = 400;
            $response['error'] = true;
            return $response;
        } elseif ($this->operatorModel->getByColumn($data)) {
            $response['message'] = 'Ya hay un operador con ese nombre registrado.';
            $response['status'] = 400;
            $response['error'] = true;
            return $response;
        }
        
        
        if (!$this->operatorModel->create($data)) {
            $response['message'] = 'Ha ocurrido un error con el registro del operador. Por favor, avise esto al administrador del sistema.';
            $response['status'] = 400;
            $response['error'] = true;
        } else {
            $response['message'] = 'El registro ha sido exitoso.';
            $response['status'] = 201;
            $response['error'] = false;
        }
        return $response;
    }
    
    function getOperators() {        
        $response = $this->operatorModel->getAll();
        $response['status'] = ($response)? 200 : 400;        
        return $response;
    }
}
