<?php //
namespace recargaonline;
ini_set( 'session.use_only_cookies', TRUE ); //Trying to avoid XXS				
ini_set( 'session.use_trans_sid', FALSE );
ini_set ('session.cookie_httponly', TRUE); // cookie as accessible only through the HTTP protocol
ini_set ('session.use_strict_mode', TRUE);
//ini_set('session.cookie_secure', TRUE);

include_once __DIR__.'/config/app.php';
include_once 'vendor/Slim/Slim.php';
\Slim\Slim::registerAutoloader();

$app = new \Slim\Slim();
$app->response->headers->set('Content-Type', 'application/json');
$request = substr($app->request()->getResourceUri(), strlen(ROOT.'/')); //filter_input(INPUT_SERVER, 'REQUEST_URI');

/*$var = null;
$userCookie = filter_input(INPUT_COOKIE, 'UUID4');
if (!isset($userCookie)) {
    $httpUser = filter_input(INPUT_SERVER, 'HTTP_USER_AGENT');
    $httpAccept = filter_input(INPUT_SERVER, 'HTTP_ACCEPT');
    $remoteAddress = filter_input(INPUT_SERVER, 'REMOTE_ADDR');
    $key = $httpUser . $httpAccept . $remoteAddress;
    $var = hash('sha256', $key);
    setcookie('UUID4', $var, time() + 3600, null, null, false, true);
}

var_dump($_COOKIE);*/
//echo "var= $var COOKIE = $_COOKIE[UUID4] ";


// POST route
$app->post(ROOT.'/users/:id',
    function ($id) {
        echo "This is a POST route $id ";
    }
);
$app->get(ROOT.'/users/:id',
    function ($id = '') {
        echo json_encode("This is a GET route $id ");
    }
);
$app->get(ROOT.'/users/',
    function ($id = '') {
        $user = ['user' => "Prueba $id", 'type_request' => 'GET_2 Route'];
        echo json_encode($user);
    }
);

class Router {
    
    var $key;
    
    public function __construct() {
        $userCookie = filter_input(INPUT_COOKIE, 'UUID');
        if (!isset($userCookie)) {
            $httpUser = filter_input(INPUT_SERVER, 'HTTP_USER_AGENT');
            $httpAccept = filter_input(INPUT_SERVER, 'HTTP_ACCEPT');
            $remoteAddress = filter_input(INPUT_SERVER, 'REMOTE_ADDR');
            $key = $httpUser.$httpAccept.$remoteAddress.rand(PHP_INT_MIN, PHP_INT_MAX);
            $this->key = hash('sha256', $key);
            setcookie('UUID', $this->key, time() + 3600, null, null, false, true);
        }
    }

    /**
     * Adding Middle Layer to authenticate every request
     * Checking if the request has valid api key in the 'Authorization' header
     */
    function authenticate(\Slim\Route $route) {
        // Getting request headers
        $headers = apache_request_headers();
        $response = array();
        $app = \Slim\Slim::getInstance();

        // Verifying Authorization Header
        if (isset($headers['Authorization'])) {
            $db = new DbHandler();

            // get the api key
            $api_key = $headers['Authorization'];
            // validating api key
            if (!$db->isValidApiKey($api_key)) {
                // api key is not present in users table
                $response["error"] = true;
                $response["message"] = "Access Denied. Invalid Api key";
                echoRespnse(401, $response);
                $app->stop();
            } else {
                global $user_id;
                // get user primary key id
                $user = $db->getUserId($api_key);
                if ($user != NULL)
                    $user_id = $user["id"];
            }
        } else {
            // api key is missing in header
            $response["error"] = true;
            $response["message"] = "Api key is misssing";
            echoRespnse(400, $response);
            $app->stop();
        }
    }

}


$app->run();

//include_once __DIR__.'/config/app.php';
//foreach (glob('controllers/*.php') as $filename)
//{
//    include_once __DIR__."/$filename";
//}
//
//class Router {
//    
//    public function __construct($controller = '', $id = '', $method = '') {
//        header("Access-Control-Allow-Orgin: *");
//        header("Access-Control-Allow-Methods: *");
//        header("Content-Type: application/json");  
//        $class = '\mirecarga\\'.\substr($controller, 0, -1).'Controller';
//        $object = new $class;
//        echo json_encode($object->get($id));
//    }
//}
//
//$method = strtolower(filter_input(INPUT_SERVER, 'REQUEST_METHOD'));
//$request = filter_input(INPUT_SERVER, 'REQUEST_URI');
//$requestString = substr($request, strlen(ROOT.'/'));
//$urlParams = explode('/', $requestString);
//
//$controllerName = array_shift($urlParams);
//$actionName = array_shift($urlParams);
//
//$router = new Router($controllerName, $actionName, $method);