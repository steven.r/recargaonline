angular.module('OnlineRechargeApp')
        .controller("LoginController", ['$http', '$q', '$location', LoginController]);

function LoginController($http, $q, $location)
{
    //Attributes
    var scope = this;
    //Methods

    scope.isLogged = function () {
        var cookies = document.cookie.split(';');
        // Now take key value pair out of this array
        var logged = false;
        for (var i = 0; i < cookies.length; i++) {
            var key = cookies[i].split('=')[0].trim();
            var value = cookies[i].split('=')[1];
            if (key === 'UUID' && value !== '') {
                logged = true;
            }
        }
        return logged;
    };

    scope.login = function () {
        $http({
            url: root + "/login",
            method: "POST",
            dataType: 'json',
            data: {"email": scope.email || '', "password": scope.password}
        }).success(function (data, status, headers, config) {
            if (!data.error) {
                $location.path("/home"); // path not hash
            } else {
                alert(data.message);
            }
        }).error(function (data, status, headers, config) {
            alert(data.message);
        });
    };

    scope.logout = function () {
        $http({
            url: root + "/logout",
            method: "POST"
        }).success(function (data, status, headers, config) {
            if (!data.error) {
                $location.path("/"); // path not hash
            } else {
                alert(data.message);
            }
        }).error(function (data, status, headers, config) {
            alert(data.message);
        });
    };
}
;