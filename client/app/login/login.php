<div ng-controller="LoginController as LoginCtrl">
    <div class="row">
        <div class="col-xs-12">
            <br><br>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-4 col-md-offset-4">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title text-center"><b>Iniciar Sesión</b></h3>
                </div>
                <div class="panel-body">
                    <form name="formLogin">
                        <div class="form-group label-floating is-empty">
                            <label class="control-label" for="email">Email</label>
                            <input ng-model="LoginCtrl.email" type="email" class="form-control" id="email" name="email" required/>
                            <div class="text-danger" ng-messages="formLogin.email.$error" ng-if="formLogin.email.$dirty">                                
                                <div ng-message="email">El email es inválido</div>
                            </div>
                        </div>                           
                        <div class="form-group label-floating is-empty">
                            <label class="control-label" for="password">Contraseña</label>
                            <input ng-model="LoginCtrl.password" type="password" class="form-control" id="password" name="password">
                        </div>                            
                    </form>
                </div>
                <div class="panel-footer text-right">
                    <div class="row">
                        <div class="col-xs-10 col-xs-offset-1">
                            <button type="submit" class="btn btn-success btn-raised btn-block" ng-click="LoginCtrl.login();">
                                Iniciar
                            </button>
                        </div>
                    </div>    
                </div>
            </div>
            <div>
                ¿Aún no tienes cuenta? <a href="#/register">Registrate</a>
            </div>
        </div>            
    </div>
</div>