<div ng-controller="RechargeController as RechargeCtrl">
    <div class="row">
        <div class="col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
            <div class="panel panel-primary" ng-init="RechargeCtrl.getRechargesRecord();">
                <div class="panel-heading">
                    <h3 class="panel-title text-center"><b>Historial de recargas</b></h3>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table panel panel-primary table-hover table-bordered">
                            <thead class="panel-heading">
                                <tr>
                                    <th class="text-center">#</th>
                                    <th class="text-center">Número</th>
                                    <th class="text-center">Saldo</th>
                                    <th class="text-center">Fecha</th>
                                </tr>
                            </thead>
                            <tbody>
                                <!--<tr ng-repeat="recordRecharge in RechargeCtrl.rechargesRecord | filter:q as results">-->
                                <tr ng-repeat="recordRecharge in RechargeCtrl.rechargesRecord">
                                    <td class="text-center">{{$index + 1}}</td>
                                    <td class="text-center">{{recordRecharge.number}}</td>
                                    <td class="text-right">{{recordRecharge.balance}}</td>
                                    <td class="text-right">{{recordRecharge.createddate}}</td>
                                </tr>                                
                            </tbody>                           
                        </table>                        
                    </div>
                    <div class="row" ng-if="RechargeCtrl.rechargesRecord.length === 0">
                        <div class="col-xs-12">
                            <div class="alert alert-info">                    
                                <strong>No hay resultados.</strong> 
                            </div>
                        </div>    
                    </div>
                </div>
            </div>
        </div>        
    </div>
</div>