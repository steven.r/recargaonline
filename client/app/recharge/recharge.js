angular.module('OnlineRechargeApp')
        .controller("RechargeController", ['$http', '$q', '$location', RechargeController]);

function RechargeController($http, $q, $location)
{
    //Attributes
    var scope = this;
    //Methods

    scope.doRecharge = function () {
        $http({
            url: root + '/recharges'
            , method: "POST"
            , dataType: 'json'
            , data: scope.rechargeForm
        }).success(function (data, status, headers, config) {
            if (!data.error) {
                alert(data.message);
                document.rechargeForm.reset();
                //$location.path("/home"); // path not hash
            } else {
                alert(data.message);
            }
        }).error(function (data, status, headers, config) {
            alert(data.message);
        });
    };

    scope.getRechargesRecord = function () {
        $http({
            url: root + '/recharges'
            , method: "GET"
        }).success(function (data, status, headers, config) {
            console.log(data);
            scope.rechargesRecord = data;
            /*if (!data.error) {
                //alert(data.message);
                //document.rechargeForm.reset();
                //$location.path("/home"); // path not hash
                scope.rechargesRecord = data;
            } else {
                alert(data.message);
            }*/
        }).error(function (data, status, headers, config) {
            alert(data.message);
        });
    }
}
;