<div ng-controller="RechargeController as RechargeCtrl">
    <div class="row">
        <div class="col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title text-center"><b>Recargas</b></h3>
                </div>
                <div class="panel-body">
                    <form name="rechargeForm">
                        <fieldset>
                            <div class="form-group label-floating is-empty">
                                <label for="phoneNumber" class="control-label">Número de celular</label>
                                <input type="number" class="form-control" ng-model="RechargeCtrl.rechargeForm.phoneNumber" id="phoneNumber" name="phoneNumber" min="0" step="1">
                            </div>
                            <div class="form-group label-floating is-empty">
                                <label for="balance" class="control-label">Valor de la recarga</label>
                                <input type="number" class="form-control" ng-model="RechargeCtrl.rechargeForm.balance" id="balance" name="balance" min="0" step="1">
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-6 col-md-offset-3">
                                    <button ng-click="RechargeCtrl.doRecharge();" ng-disabled="rechargeForm.$invalid" type="button"class="btn btn-primary btn-block btn-raised" type="button">
                                        Hacer recarga
                                    </button>
                                </div>                                     
                            </div> 
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>

</div>