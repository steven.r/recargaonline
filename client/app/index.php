<!DOCTYPE html>
<html lang="es" ng-app="OnlineRechargeApp">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Recarga Online</title>

        <!-- Bootstrap -->
        <link href="../css/bootstrap.min.css" rel="stylesheet">
        <link href="../css/bootstrap-material-design.min.css" rel="stylesheet">        
        <link href="../css/ripples.min.css" rel="stylesheet">         
        <link href="../css/style.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>        
        <div class="container">
            <header>
                <div ng-include="'navbar/navbar.php'"></div>
            </header>
            <div id="main">
                <div ng-view>                
                </div>
            </div>
        </div>    
        <script src="../vendor/jquery/jquery.min.js"></script>
        <script src="../vendor/bootstrap/bootstrap.min.js"></script>
        <script src="../vendor/material-bootstrap/material.min.js"></script>
        <script src="../vendor/material-bootstrap/ripples.min.js"></script>
        <script src="../vendor/angular/angular.min.js"></script>        
        <script src="../vendor/angular/angular-messages.min.js"></script>
        <script src="../vendor/angular/angular-route.min.js"></script>
        <script src="script.js"></script>
        <script src="login/login.js"></script>
        <script src="user/user.js"></script>
        <script src="recharge/recharge.js"></script>        
        <script src="operator/operator.js"></script>
        <script src="plan/plan.js"></script>
        <script src="phonenumber/phonenumber.js"></script>
        <script src="comsuption/comsuption.js"></script>
        <!--<script>window.onload = function () {$.material.init();};</script>-->
    </body>
</html>