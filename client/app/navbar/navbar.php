<div ng-controller="LoginController as LoginCtrl">
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navoption" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#/home">Recarga Online</a>
            </div>
            <div class="collapse navbar-collapse" id="navoption" ng-if="LoginCtrl.isLogged();">
                <ul class="nav navbar-nav">
                    <li> <a href="#/home">Inicio</a></li>                    
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Recargas<span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="#/recharges">Hacer recarga</a></li>
                            <li><a href="#/rechargesrecord">Historial de recargas</a></li>
                            <li><a href="#/balancePhoneNumber">Ver saldos</a></li>
                        </ul>
                    </li>         
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Consumos <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="#/registerComsuption">Registrar consumo</a></li>
                            <li><a href="#/registeredComsuption">Historial de consumos</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Registros<span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="#/registerPhoneNumber">Registrar número</a></li>
                            <li><a href="#/registeredPhoneNumber">Números registrados</a></li>
                            <li><a href="#/registerOperator">Registrar operador</a></li>
                            <li><a href="#/registeredOperator">Operadores registrados</a></li>
                            <li><a href="#/registerPlan">Registrar plan</a></li>
                            <li><a href="#/registeredPlan">Planes registrados</a></li>
                            <li><a href="#/registeredPlanDetail">Ver detalles de planes registrados</a></li>
                        </ul>
                    </li>
                </ul>            
                <ul class="nav navbar-nav navbar-right">                
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Sesión<span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a>Perfil</a></li>
                            <li role="separator" class="divider"></li>
                            <li>
                                <div class="row-content">
                                    <div class="col-xs-12">
                                        <button type="button" ng-click="LoginCtrl.logout();" class="btn btn-danger btn-block btn-raised">
                                            <span class="glyphicon glyphicon-off" aria-hidden="true"></span> Cerrar sesión
                                        </button>  
                                    </div>
                                </div>                            
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </nav>    
</div>