<div ng-controller="UserController as UserCtrl">
    <div class="row">
        <div class="col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title text-center">
                        <b>Formulario de registro</b>
                    </h3>
                </div>
                <div class="panel-body">
                    <form name="registerForm">
                        <div class="form-group label-floating is-empty">
                            <label class="control-label" for="email">Email</label>
                            <input type="email" class="form-control" ng-model="UserCtrl.registerForm.email" id="email" name="email" required/>
                            <div class="text-danger" ng-messages="registerForm.email.$error" ng-if="registerForm.email.$dirty">
                                <div ng-message="required">Este campo es requerido</div>
                                <div ng-message="email">El email es inválido</div>
                            </div>
                        </div>
                        <div class="form-group label-floating is-empty">
                            <label class="control-label" for="password">Contraseña</label>
                            <input type="password" class="form-control" ng-model="UserCtrl.registerForm.password" id="password" name="password" required/>
                            <div class="text-danger" ng-messages="registerForm.password.$error" ng-if="registerForm.password.$dirty">
                                <div ng-message="required">Este campo es requerido</div>
                                <div ng-message="pattern">Las contraseñas no coinciden</div>
                            </div>
                        </div>
                        <div class="form-group label-floating is-empty">
                            <label class="control-label" for="verifyPassword">Repita su contraseña</label>
                            <input type="password" class="form-control" ng-model="UserCtrl.registerForm.verifyPassword" ng-pattern="{{UserCtrl.registerForm.password}}" 
                                   id="verifyPassword" name="verifyPassword" required/>
                            <div class="text-danger" ng-messages="registerForm.verifyPassword.$error" ng-if="registerForm.verifyPassword.$dirty">
                                <div ng-message="required">Este campo es requerido</div>
                                <div ng-message="pattern">Deben coincidir las contraseñas</div>
                            </div>
                        </div>   
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" ng-model="UserCtrl.registerForm.acceptTerm" id="acceptTerm" name="acceptTerm" required/>
                                <span class="checkbox-material">
                                    <span class="check"></span>                                    
                                </span> Acepto términos y condiciones
                            </label>
                            <div class="text-danger" ng-messages="registerForm.acceptTerm.$error" ng-if="registerForm.acceptTerm.$dirty">
                                <div ng-message="required">Este campo es requerido</div>
                            </div>
                        </div>
                        <div class="row-content">
                            <button type="button" ng-click="UserCtrl.register();" ng-disabled="registerForm.$invalid" class="btn btn-primary btn-raised col-xs-12" type="button">
                                Registrarme
                            </button>                 
                        </div>                        
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>