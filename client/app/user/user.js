angular.module('OnlineRechargeApp')
        .controller("UserController", ['$http', '$q', '$location', UserController]);

function UserController($http, $q, $location)
{
    //Attributes
    var scope = this;
    scope.email = null;
    //Methods
    
    scope.register = function () {
        console.log(scope.registerForm);
        $http({
            url: root+'/users'
            ,method: "POST"
            ,dataType: 'json'
            ,data: scope.registerForm           
        }).success(function (data, status, headers, config) {            
            if (!data.error) {                
                alert(data.message);
                //$location.path("/home"); // path not hash
            } else {                
                alert(data.message);
            }
        }).error(function (data, status, headers, config) {            
            alert(data.message);
        });
    };
    
    scope.get = function () {
        $http({
            url: root+'/users'
            ,method: "GET"
            /*,params: {user_id: user.id}*/
        }).success(function (data, status, headers, config) {            
            if (!data.error) {                
                scope.email = data.email;
                //$location.path("/home"); // path not hash
            } else {                
                alert(data.message);
            }
        }).error(function (data, status, headers, config) {            
            alert(data.message);
        });
    };
};