switch (document.location.hostname)
{
    case 'localhost' :
    case '127.0.0.1' :
        var rootFolder = '/recargaonline/api';
        break;
    default :
        var rootFolder = '/api';
        break;
}
var root = rootFolder;//document.location.hostname + rootFolder;
// create the module and name it OnlineRechargeApp
// also include ngRoute for all our routing needs
var OnlineRechargeApp = angular.module('OnlineRechargeApp', ['ngRoute', 'ngMessages']);

// configure our routes
OnlineRechargeApp
        .config(function ($routeProvider) {
            $routeProvider
                    // route for the home page
                    .when('/', {
                        templateUrl: 'login/login.php'
                        ,controller: 'LoginController'
                    })
                    .when('/home', {
                        templateUrl: 'home/home.php'
                        ,controller: 'LoginController'
                    })
                    .when('/register', {
                        templateUrl: 'user/createUser.php'
                        ,controller: 'UserController'
                    })
                    .when('/recharges', {
                        templateUrl: 'recharge/doRecharge.php'
                        //,controller: 'userController'
                    })
                    .when('/rechargesrecord', {
                        templateUrl: 'recharge/rechargesRecord.php'
                        //,controller: 'userController'
                    })
                    .when('/registerPhoneNumber', {
                        templateUrl: 'phonenumber/registerPhoneNumber.php'
                        //,controller: 'userController'
                    })
                    .when('/registeredPhoneNumber', {
                        templateUrl: 'phonenumber/registeredPhoneNumber.php'
                        //,controller: 'userController'
                    })
                    .when('/registerOperator', {
                        templateUrl: 'operator/registerOperator.php'
                        //,controller: 'userController'
                    })
                    .when('/registeredOperator', {
                        templateUrl: 'operator/registeredOperator.php'
                        //,controller: 'userController'
                    })
                    .when('/registerPlan', {
                        templateUrl: 'plan/registerPlan.php'
                        //,controller: 'userController'
                    })
                    .when('/registeredPlan', {
                        templateUrl: 'plan/registeredPlan.php'
                        //,controller: 'userController'
                    })
                    .when('/balancePhoneNumber', {
                        templateUrl: 'phonenumber/viewBalancePhoneNumber.php'
                        //,controller: 'userController'
                    })
                    .when('/registerComsuption', {
                        templateUrl: 'comsuption/registerComsuption.php'
                        //,controller: 'userController'
                    })
                    .when('/registeredComsuption', {
                        templateUrl: 'comsuption/registeredComsuption.php'
                        //,controller: 'userController' 
                    })
                    .when('/registeredPlanDetail', {
                        templateUrl: 'plan/registeredPlanDetail.php'
                        //,controller: 'userController' registeredPlanDetail
                    });
        })
        .run(function ($rootScope, $location) {
            // register listener to watch route changes
            $rootScope.$on("$routeChangeStart", function (event, next, current) {
                var cookies = document.cookie.split(';');
                // Now take key value pair out of this array
                var logged = false;
                for (var i = 0; i < cookies.length; i++) {
                    var key = cookies[i].split('=')[0].trim();
                    var value = cookies[i].split('=')[1];
                    if (key === 'UUID' && value !== '') {
                        logged = true;
                    }
                }
                if (logged === true && next.templateUrl === "login/login.php") {
                    $location.path("/home"); // path not hash
                    
                    /*if ($rootScope.loggedUser == null) {
                        // no logged user, we should be going to #login
                        if (next.templateUrl == "partials/login.html") {
                            // already going to #login, no redirect needed
                        } else {
                            // not going to #login, we should redirect now
                            $location.path("/login");
                        }
                    }*/
                    //$location.path("/home"); // path not hash
                } else if (logged !== true && next.templateUrl !== "user/createUser.php") {
                    $location.path("/");
                }
                $.material.init(); //Execute this line every page change due to error to display material effects
            });
        });