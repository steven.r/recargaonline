<div ng-controller="PlanController as PlanCtrl">
    <div class="row">
        <div class="col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
            <div class="panel panel-primary" ng-init="PlanCtrl.getOperators();">
                <div class="panel-heading">
                    <h3 class="panel-title text-center"><b>Registrar plan</b></h3>
                </div>
                <div class="panel-body">
                    <form name="registerPlanForm">
                        <fieldset>
                            <div class="form-group label-floating is-empty">
                                <label for="operatorName" class="control-label">Nombre del plan</label>
                                <input type="text" class="form-control" ng-model="PlanCtrl.registerPlanForm.planName" id="planName" name="planName" required>
                            </div>
                            <div class="form-group">
                                <label for="operator" class="control-label">Operador</label>
                                <div>
                                    <select ng-model="PlanCtrl.registerPlanForm.operator" id="operator" name="operator" class="form-control" 
                                            ng-options="operator.id as operator.name for operator in PlanCtrl.operators track by operator.id" required>                                        
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <h4>Costo por segundo</h4>
                            </div>
                            <div class="form-group label-floating is-empty" ng-repeat="operator in PlanCtrl.operators">
                                <label for="operator" class="control-label">{{operator.name}}</label>                                
                                <input type="number" class="form-control" ng-model="PlanCtrl.registerPlanForm.costPerSec[operator.id]" id="balance" name="balance" min="0" step="any" required>                           
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-6 col-md-offset-3">
                                    <button ng-click="PlanCtrl.registerPlan();" ng-disabled="registerPlanForm.$invalid" type="button"class="btn btn-primary btn-block btn-raised" type="button">
                                        Registrar
                                    </button>
                                </div>                                     
                            </div> 
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

