<div ng-controller="PlanController as PlanCtrl">
    <div class="row">
        <div class="col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-2 col-md-8 col-md-offset-2">
            <div class="panel panel-primary" ng-init="PlanCtrl.getOperators();">
                <div class="panel-heading">
                    <h3 class="panel-title text-center"><b>Detalles de plan</b></h3>
                </div>
                <div class="panel-body">
                    <form id="searchPlanDetailForm" name="searchPlanDetailForm">
                        <div class="form-group">
                            <label for="operator" class="control-label">Operador</label>
                            <div>
                                <select ng-model="PlanCtrl.searchPlanDetailForm.operator" id="operator" name="operator" class="form-control" 
                                        ng-options="operator.id as operator.name for operator in PlanCtrl.operators track by operator.id" 
                                        ng-change="PlanCtrl.getPlansByOperator();" required>                                        
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="plan" class="control-label">Plan</label>
                            <div>
                                <select ng-model="PlanCtrl.searchPlanDetailForm.plan" id="plan" name="plan" class="form-control" 
                                        ng-options="plan.id as plan.name for plan in PlanCtrl.plans track by plan.id" required>                                        
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-md-6 col-md-offset-3">
                                <button ng-click="PlanCtrl.getPlanDetail();" ng-disabled="searchPlanDetailForm.$invalid" type="button"class="btn btn-primary btn-block btn-raised" type="button">
                                    Buscar
                                </button>
                            </div>                                     
                        </div>
                    </form>
                    <div class="row">
                        <div class="col-xs-12">
                            <hr>
                        </div>                 
                    </div>
                    <div ng-if="PlanCtrl.planDetail.length > 0 && PlanCtrl.planDetail !== 'undefined'" class="table-responsive">
                        <table class="table panel panel-primary table-hover table-bordered">
                            <thead class="panel-heading">
                                <tr>
                                    <th class="text-center">#</th>                                    
                                    <th class="text-center">Operador</th>
                                    <th class="text-center">Plan</th>
                                    <th class="text-center">Operador Destino</th>
                                    <th class="text-center">Costo/segundo</th>
                                </tr>
                            </thead>
                            <tbody>
                                <!--<tr ng-repeat="recordRecharge in RechargeCtrl.rechargesRecord | filter:q as results">-->
                                <tr ng-repeat="planDetail in PlanCtrl.planDetail">
                                    <td rowspan="{{PlanCtrl.planDetail.length}}" ng-hide="$index > 0" class="text-center">{{$index + 1}}</td>                                    
                                    <td rowspan="{{PlanCtrl.planDetail.length}}" ng-hide="$index > 0" class="text-center">{{planDetail.operator_name}}</td>
                                    <td rowspan="{{PlanCtrl.planDetail.length}}" ng-hide="$index > 0" class="text-right">{{planDetail.plan_name}}</td>
                                    <td class="text-center">{{planDetail.destination_operator_name}}</td>
                                    <td class="text-right">{{planDetail.cost}}</td>
                                </tr>                                
                            </tbody>                           
                        </table>                        
                    </div>
                    <div class="row" ng-if="PlanCtrl.planDetail.length === 0">
                        <div class="col-xs-12">
                            <div class="alert alert-info">                    
                                <strong>No hay resultados.</strong> 
                            </div>
                        </div>    
                    </div>
                </div>
            </div>
        </div>        
    </div>
</div>