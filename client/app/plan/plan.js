angular.module('OnlineRechargeApp')
        .factory('PlanService',['$http', function ($http) {
            return({
                getPlans: getPlans
            });
            function getPlans(operator) {
                operator = typeof operator !== 'undefined' ? operator : null;
                var operatorRoot = (operator !== null)? '/'+operator: '';
                return $http({
                    url: root + '/plans'+operatorRoot
                    ,method: "GET"
                });
            };
        }])
        .controller("PlanController", ['$http', '$q', '$location', 'OperatorService' ,'PlanService', PlanController]);

function PlanController($http, $q, $location, OperatorService, PlanService)
{
    //Attributes
    var scope = this;
    //Methods

    scope.registerPlan = function () {
        var costOperatorFoo = [];
        
        for (var costOperator in scope.registerPlanForm.costPerSec) {
            costOperatorFoo.push({'operator_id': costOperator, 
                'cost': scope.registerPlanForm.costPerSec[costOperator]});
        }
        var data = {'planName': scope.registerPlanForm.planName
                    ,'operator': scope.registerPlanForm.operator
                    ,'costOperator': costOperatorFoo};
        $http({
            url: root + '/plans'
            , method: "POST"
            , dataType: 'json'
            , data: data
        }).success(function (data, status, headers, config) {
            if (!data.error) {
                alert(data.message);
                document.registerPlanForm.reset();
                //$location.path("/home"); // path not hash
            } else {
                alert(data.message);
            }
        }).error(function (data, status, headers, config) {
            alert(data.message);
        });
    };

    scope.getRegisteredPlan = function () {
        var planService = PlanService.getPlans();
        planService.success(function (data, status, headers, config) {            
            scope.plans = data;
        }).error(function (data, status, headers, config) {
            alert(data.message);
        });
        /*$http({
            url: root + '/plans'
            , method: "GET"
        }).success(function (data, status, headers, config) {
            console.log(data);
            scope.plans = data;
        }).error(function (data, status, headers, config) {
            alert(data.message);
        });*/
    }
    scope.getOperators = function() {
        var operatorService = OperatorService.getOperators();
        operatorService.success(function (data, status, headers, config) {            
            scope.operators = data;
        }).error(function (data, status, headers, config) {
            alert(data.message);
        });
    }
    
    scope.getPlansByOperator = function () {
        var planService = PlanService.getPlans(scope.searchPlanDetailForm.operator);
        planService.success(function (data, status, headers, config) {
            scope.plans = data;
        }).error(function (data, status, headers, config) {
            alert(data.message);
        });
    }

    scope.getPlanDetail = function () {
        var planId = (scope.searchPlanDetailForm.plan !== null) ? '/' + scope.searchPlanDetailForm.plan : '';
        $http({
            url: root + '/plans' + planId + '/details'
            , method: "GET"
        }).success(function (data, status, headers, config) {            
            scope.planDetail = data;
        }).error(function (data, status, headers, config) {
            alert(data.message);
        });
    }
}
;