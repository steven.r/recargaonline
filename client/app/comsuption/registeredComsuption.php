<div ng-controller="ComsuptionController as ComsuptionCtrl">
    <div class="row">
        <div class="col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-2 col-md-6 col-md-offset-3">
            <div class="panel panel-primary" ng-init="ComsuptionCtrl.getRegisteredComsuptions();">
                <div class="panel-heading">
                    <h3 class="panel-title text-center"><b>Consumos registrados</b></h3>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table panel panel-primary table-hover table-bordered">
                            <thead class="panel-heading">
                                <tr>
                                    <th class="text-center">#</th>
                                    <th class="text-center">Número origen</th>
                                    <th class="text-center">Número destino</th>
                                    <th class="text-center">Duracion de la llamada</th>
                                    <th class="text-center">Saldo gastado</th>
                                    <th class="text-center">Fecha de consumo</th>
                                </tr>
                            </thead>
                            <tbody>
                                <!--<tr ng-repeat="recordRecharge in RechargeCtrl.rechargesRecord | filter:q as results">-->
                                <tr ng-repeat="comsuption in ComsuptionCtrl.comsuptions">
                                    <td class="text-center">{{$index + 1}}</td>
                                    <td class="text-center">{{comsuption.origin_phonenumber}}</td>
                                    <td class="text-right">{{comsuption.destiny_phonenumber}}</td>
                                    <td class="text-center">{{comsuption.call_duration}}</td>
                                    <td class="text-right">{{comsuption.balance_spent}}</td>
                                    <td class="text-right">{{comsuption.createddate}}</td>
                                </tr>                                
                            </tbody>                           
                        </table>                        
                    </div>
                    <div class="row" ng-if="PhoneNumberCtrl.phoneNumbers.length === 0">
                        <div class="col-xs-12">
                            <div class="alert alert-info">                    
                                <strong>No hay resultados.</strong> 
                            </div>
                        </div>    
                    </div>
                </div>
            </div>
        </div>        
    </div>
</div>