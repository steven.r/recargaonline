angular.module('OnlineRechargeApp')
        .controller("ComsuptionController", ['$http', '$q', '$location', ComsuptionController]);

function ComsuptionController($http, $q, $location)
{
    //Attributes
    var scope = this;
    //Methods

    scope.registerComsuption = function () {
        $http({
            url: root + '/comsuptions'
            , method: "POST"
            , dataType: 'json'
            , data: scope.registerComsuptionForm
        }).success(function (data, status, headers, config) {
            if (!data.error) {
                alert(data.message);
                document.registerComsuptionForm.reset();
                //$location.path("/home"); // path not hash
            } else {
                alert(data.message);
            }
        }).error(function (data, status, headers, config) {
            alert(data.message);
        });
    };

    scope.getRegisteredComsuptions = function () {
        $http({
            url: root + '/comsuptions'
            , method: "GET"
        }).success(function (data, status, headers, config) {
            scope.comsuptions = data;
        }).error(function (data, status, headers, config) {
            alert(data.message);
        });
    }
}
;