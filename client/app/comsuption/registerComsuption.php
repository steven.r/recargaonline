<div ng-controller="ComsuptionController as ComsuptionCtrl">
    <div class="row">
        <div class="col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title text-center"><b>Consumos</b></h3>
                </div>
                <div class="panel-body">
                    <form id="registerComsuptionForm" name="registerComsuptionForm">
                        <fieldset>
                            <div class="form-group label-floating is-empty">
                                <label for="phoneNumber" class="control-label">Número de celular origen</label>
                                <input type="number" class="form-control" ng-model="ComsuptionCtrl.registerComsuptionForm.originPhoneNumber" id="originPhoneNumber" name="originPhoneNumber" min="0" step="1" required>
                            </div>
                            <div class="form-group label-floating is-empty">
                                <label for="phoneNumber" class="control-label">Número de celular destino</label>
                                <input type="number" class="form-control" ng-model="ComsuptionCtrl.registerComsuptionForm.destinyPhoneNumber" id="destinyPhoneNumber" name="destinyPhoneNumber" min="0" step="1" required>
                            </div>
                            <div class="form-group is-empty">
                                <label for="phoneNumber" class="control-label">Duración de la llamada</label>
                                <div class="row">
                                    <div class="col-xs-6">
                                        <div class="form-group label-floating is-empty">
                                            <label for="minutes" class="control-label">Minutos</label>
                                            <input type="number" class="form-control" ng-model="ComsuptionCtrl.registerComsuptionForm.minutes" id="minutes" name="minutes" min="0" max="1380" step="1" required>
                                        </div>
                                    </div>                                    
                                    <div class="col-xs-6">
                                        <div class="form-group label-floating is-empty">
                                            <label for="seconds" class="control-label">Segundos</label>
                                            <input type="number" class="form-control" ng-model="ComsuptionCtrl.registerComsuptionForm.seconds" id="seconds" name="seconds" min="0" max="59" step="1" required>
                                        </div>
                                    </div>                                
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-6 col-md-offset-3">
                                    <button ng-click="ComsuptionCtrl.registerComsuption();" ng-disabled="registerComsuptionForm.$invalid" type="button"class="btn btn-primary btn-block btn-raised" type="button">
                                        Registrar
                                    </button>
                                </div>                                     
                            </div> 
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>

</div>