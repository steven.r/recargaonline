angular.module('OnlineRechargeApp')
        .controller("PhoneNumberController", ['$http', '$q', '$location', 'OperatorService', 'PlanService', PhoneNumberController]);

function PhoneNumberController($http, $q, $location, OperatorService, PlanService)
{
    //Attributes
    var scope = this;
    //Methods

    scope.registerPhoneNumber = function () {
        $http({
            url: root + '/phonenumbers'
            , method: "POST"
            , dataType: 'json'
            , data: scope.registerNumberForm
        }).success(function (data, status, headers, config) {
            if (!data.error) {
                alert(data.message);
                document.registerNumberForm.reset();
                //$location.path("/home"); // path not hash
            } else {
                alert(data.message);
            }
        }).error(function (data, status, headers, config) {
            alert(data.message);
        });
    };

    scope.getRegisteredPhoneNumber = function () {
        $http({
            url: root + '/phonenumbers'
            , method: "GET"
        }).success(function (data, status, headers, config) {
            console.log(data);
            scope.phoneNumbers = data;
            /*if (!data.error) {
             //alert(data.message);
             //document.rechargeForm.reset();
             //$location.path("/home"); // path not hash
             scope.rechargesRecord = data;
             } else {
             alert(data.message);
             }*/
        }).error(function (data, status, headers, config) {
            alert(data.message);
        });
    }
    
    scope.getOperators = function() {
        var operatorService = OperatorService.getOperators();
        operatorService.success(function (data, status, headers, config) {
            scope.operators = data;
            console.log(data);
        }).error(function (data, status, headers, config) {
            alert(data.message);
        });
        
        /*var planService = PlanService.getPlans();
        planService.success(function (data, status, headers, config) {
            scope.plans = data;
        }).error(function (data, status, headers, config) {
            alert(data.message);
        });*/ 
    };
    
    scope.getPlansByOperator = function () {
        var planService = PlanService.getPlans(scope.registerNumberForm.operator);
        planService.success(function (data, status, headers, config) {
            scope.plans = data;
        }).error(function (data, status, headers, config) {
            alert(data.message);
        });
    }
    
    scope.getBalancePhonenumber = function () {
        $http({
            url: root + '/phonenumbers/'+scope.searchNumberForm.phoneNumber
            , method: "GET"
        }).success(function (data, status, headers, config) {
            console.log(data);
            scope.phoneNumberBalance = data;
            /*if (!data.error) {
             //alert(data.message);
             //document.rechargeForm.reset();
             //$location.path("/home"); // path not hash
             scope.rechargesRecord = data;
             } else {
             alert(data.message);
             }*/
        }).error(function (data, status, headers, config) {
            alert(data.message);
        });
    }
    
}
;