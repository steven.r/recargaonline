<div ng-controller="PhoneNumberController as PhoneNumberCtrl">
    <div class="row">
        <div class="col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title text-center"><b>Registros</b></h3>
                </div>
                <div class="panel-body" ng-init="PhoneNumberCtrl.getOperators();">
                    <form name="registerNumberForm">
                        <fieldset>
                            <div class="form-group label-floating is-empty">
                                <label for="phoneNumber" class="control-label">Número de celular</label>
                                <input type="number" class="form-control" ng-model="PhoneNumberCtrl.registerNumberForm.phoneNumber" id="phoneNumber" name="phoneNumber" min="0" step="1" required>
                            </div>
                            <div class="form-group">
                                <label for="operator" class="control-label">Operador</label>
                                <div>
                                    <select ng-model="PhoneNumberCtrl.registerNumberForm.operator" id="operator" name="operator" class="form-control" 
                                            ng-options="operator.id as operator.name for operator in PhoneNumberCtrl.operators track by operator.id" 
                                            ng-change="PhoneNumberCtrl.getPlansByOperator();" required>                                        
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="plan" class="control-label">Plan</label>
                                <div>
                                    <select ng-model="PhoneNumberCtrl.registerNumberForm.plan" id="plan" name="plan" class="form-control" 
                                            ng-options="plan.id as plan.name for plan in PhoneNumberCtrl.plans track by plan.id" required>                                        
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-6 col-md-offset-3">
                                    <button ng-click="PhoneNumberCtrl.registerPhoneNumber();" ng-disabled="registerNumberForm.$invalid" type="button"class="btn btn-primary btn-block btn-raised" type="button">
                                        Registrar
                                    </button>
                                </div>                                     
                            </div> 
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>