<div ng-controller="PhoneNumberController as PhoneNumberCtrl">
    <div class="row">
        <div class="col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-2 col-md-8 col-md-offset-2">
            <div class="panel panel-primary" ng-init="">
                <div class="panel-heading">
                    <h3 class="panel-title text-center"><b>Saldos</b></h3>
                </div>
                <div class="panel-body">
                    <form id="searchNumberForm" name="searchNumberForm">
                        <div class="form-group label-floating is-empty">
                            <label for="phoneNumber" class="control-label">Número de celular</label>
                            <input type="number" class="form-control" ng-model="PhoneNumberCtrl.searchNumberForm.phoneNumber" id="phoneNumber" name="phoneNumber" min="0" step="1" required>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-md-6 col-md-offset-3">
                                <button ng-click="PhoneNumberCtrl.getBalancePhonenumber();" ng-disabled="searchNumberForm.$invalid" type="button"class="btn btn-primary btn-block btn-raised" type="button">
                                    Buscar
                                </button>
                            </div>                                     
                        </div>
                    </form>
                    <div class="row">
                        <div class="col-xs-12">
                            <hr>
                        </div>                 
                    </div>
                    <div ng-if="PhoneNumberCtrl.phoneNumberBalance.length > 0 && PhoneNumberCtrl.phoneNumberBalance !== 'undefined' " class="table-responsive">
                        <table class="table panel panel-primary table-hover table-bordered">
                            <thead class="panel-heading">
                                <tr>
                                    <th class="text-center">#</th>
                                    <th class="text-center">Número</th>
                                    <th class="text-center">Saldo</th>
                                    <th class="text-center">Operador</th>
                                    <th class="text-center">Plan</th>
                                    <th class="text-center">Operador Destino</th>
                                    <th class="text-center">Costo/segundo</th>
                                    <th class="text-center">Segundos</th>
                                </tr>
                            </thead>
                            <tbody>
                                <!--<tr ng-repeat="recordRecharge in RechargeCtrl.rechargesRecord | filter:q as results">-->
                                <tr ng-repeat="phoneNumber in PhoneNumberCtrl.phoneNumberBalance">
                                    <td rowspan="{{PhoneNumberCtrl.phoneNumberBalance.length}}" ng-hide="$index>0" class="text-center">{{$index + 1}}</td>
                                    <td rowspan="{{PhoneNumberCtrl.phoneNumberBalance.length}}" ng-hide="$index>0" class="text-center">{{phoneNumber.number}}</td>
                                    <td rowspan="{{PhoneNumberCtrl.phoneNumberBalance.length}}" ng-hide="$index>0" class="text-right">{{phoneNumber.balance}}</td>
                                    <td rowspan="{{PhoneNumberCtrl.phoneNumberBalance.length}}" ng-hide="$index>0" class="text-center">{{phoneNumber.operator_name}}</td>
                                    <td rowspan="{{PhoneNumberCtrl.phoneNumberBalance.length}}" ng-hide="$index>0" class="text-right">{{phoneNumber.plan_name}}</td>
                                    <td class="text-center">{{phoneNumber.destination_operator_name}}</td>
                                    <td class="text-right">{{phoneNumber.cost}}</td>
                                    <td class="text-right">{{phoneNumber.seconds}}</td>
                                </tr>                                
                            </tbody>                           
                        </table>                        
                    </div>
                    <div class="row" ng-if="PhoneNumberCtrl.phoneNumberBalance.length === 0">
                        <div class="col-xs-12">
                            <div class="alert alert-info">                    
                                <strong>No hay resultados.</strong> 
                            </div>
                        </div>    
                    </div>
                </div>
            </div>
        </div>        
    </div>
</div>