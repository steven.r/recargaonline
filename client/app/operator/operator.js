angular.module('OnlineRechargeApp')
        .factory('OperatorService',['$http', function ($http) {
            return({
                getOperators: getOperators
            });
            function getOperators() {
                return $http({
                    url: root + '/operators'
                    ,method: "GET"
                });
            };
        }])
        .controller("OperatorController", ['$http', '$q', '$location', 'OperatorService', OperatorController]);

function OperatorController($http, $q, $location, OperatorService)
{
    //Attributes
    var scope = this;
    //Methods

    scope.registerOperator = function () {
        $http({
            url: root + '/operators'
            , method: "POST"
            , dataType: 'json'
            , data: scope.registerOperatorForm
        }).success(function (data, status, headers, config) {
            if (!data.error) {
                alert(data.message);
                document.registerOperatorForm.reset();
                //$location.path("/home"); // path not hash
            } else {
                alert(data.message);
            }
        }).error(function (data, status, headers, config) {
            alert(data.message);
        });
    };

    scope.getRegisteredOperators = function () {
        var operatorService = OperatorService.getOperators();
        operatorService.success(function (data, status, headers, config) {            
            scope.operators = data;
        }).error(function (data, status, headers, config) {
            alert(data.message);
        });
    }
}
;