<div ng-controller="OperatorController as OperatorCtrl">
    <div class="row">
        <div class="col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title text-center"><b>Registrar operador</b></h3>
                </div>
                <div class="panel-body">
                    <form name="registerOperatorForm">
                        <fieldset>
                            <div class="form-group label-floating is-empty">
                                <label for="operatorName" class="control-label">Nombre del operador</label>
                                <input type="text" class="form-control" ng-model="OperatorCtrl.registerOperatorForm.operatorName" id="operatorName" name="operatorName" required>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-6 col-md-offset-3">
                                    <button ng-click="OperatorCtrl.registerOperator();" ng-disabled="registerOperatorForm.$invalid" type="button"class="btn btn-primary btn-block btn-raised" type="button">
                                        Registrar
                                    </button>
                                </div>                                     
                            </div> 
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>