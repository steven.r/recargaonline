<div ng-controller="OperatorController as OperatorCtrl">
    <div class="row">
        <div class="col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
            <div class="panel panel-primary" ng-init="OperatorCtrl.getRegisteredOperators();">
                <div class="panel-heading">
                    <h3 class="panel-title text-center"><b>Operadores registrados</b></h3>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table panel panel-primary table-hover table-bordered">
                            <thead class="panel-heading">
                                <tr>
                                    <th class="text-center">#</th>
                                    <th class="text-center">Nombre</th>
                                    <th class="text-center">Fecha de creación</th>
                                </tr>
                            </thead>
                            <tbody>
                                <!--<tr ng-repeat="recordRecharge in RechargeCtrl.rechargesRecord | filter:q as results">-->
                                <tr ng-repeat="operator in OperatorCtrl.operators">
                                    <td class="text-center">{{$index + 1}}</td>
                                    <td class="text-center">{{operator.name}}</td>
                                    <td class="text-right">{{operator.createddate}}</td>
                                </tr>                                
                            </tbody>                           
                        </table>                        
                    </div>
                    <div class="row" ng-if="OperatorCtrl.operators.length === 0">
                        <div class="col-xs-12">
                            <div class="alert alert-info">                    
                                <strong>No hay resultados.</strong> 
                            </div>
                        </div>    
                    </div>
                </div>
            </div>
        </div>        
    </div>
</div>